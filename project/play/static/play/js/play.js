function getCellClass(n) {
    switch (n) {
        case 0: return "gray";
        case 2: return "orange";
        case 4: return "darkorange";
        case 8: return "pink_orange";
        case 16: return "light";
        case 32: return "orange";
        case 64: return "orange";
        case 128: return "orange";
        case 256: return "orange";
        case 512: return "orange";
        case 1024: return "orange";
        case 2048: return "orange";
        default: return n > 2048 ? "black" : "what";
    }
}

function RandomCell(){
    var flag = true;
            while (flag){
                var rand_i = _.random(0, 3);
                var rand_j = _.random(0, 3);
                if (window.field[rand_i][rand_j] == 0){
                    window.field[rand_i][rand_j] =Math.random() < 0.9 ? 2 : 4;
                    flag = false;
                }
            }
}
function Looser(field){
    var check = 0;
    var choice = 0;
    for (var i = 0; i < 4; i++){
                for(var j = 0; j < 4; j++) {
                    if (field[i][j] == 0){
                        check ++;
                    }
                    }
                }
    if (check == 0){
        console.log('eep');
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 3; j++) {
            if (field[i][j] == field[i][j+1] || field[j][i] == field[j+1][i]){
                choice ++;
            }
        }
    }
    }
    if (check == 0 && choice == 0){
        var html = $('<p> Game over </p>');
        $("div.square").after(html);
    //    console.log('new game');
    //    for (var i = 0; i < 4; i++){
    //        for (var j = 0; j < 4; j++){
    //            field[i][j] = 0;
    //        }
    //    }
        //StartData(field)
    }
}
function Transp(matr){
    var arr = new Array();
    for(var i=0; i<4; i++){
        arr[i] = new Array();
    for(var j=0; j<4; j++){
      arr[i][j] = 0;
    }
  }
    for (var i=0; i<4; i++){
        for (var j=0; j<4; j++){
            arr[i][j]=matr[j][i];
        }
    }
    return arr
}

function UpdateCalls(){
    var template = _.template($("#field_template").html());
    $("#field").html(template({
        "field": window.field
    }));
}

function StartData(field){
    var rand_i = _.random(0, 3);
    var rand_j = _.random(0, 3);
    var rand_i1 = _.random(0, 3);
    var rand_j1 = _.random(0, 3);
    var flag = true;
    while (flag){
        if (rand_i == rand_i1 && rand_j == rand_j1){
            rand_i1 = _.random(0, 3);
            rand_j1 = _.random(0, 3);
        }
        else{
            flag =false;
        }
    }
    field[rand_i][rand_j] =Math.random() < 0.9 ? 2 : 4;
    field[rand_i1][rand_j1] =Math.random() < 0.9 ? 2 : 4;
    UpdateCalls();
}
function LeftUp(field){

    for (var i = 0; i < 4; i++){
        for(var j = 0; j < 4; j++) {
            for (var k = j + 1; k < 4; k++)
            {
                if (field[i][k] != 0)
                {
                    if (field[i][j] == 0)
                    {
                        field[i][j] = field[i][k];
                        field[i][k] = 0;
                        window.count++;
                    }
                    else
                    {
                        if (field[i][j] ==field[i][k])
                        {
                            field[i][j] += field[i][k];
                            field[i][k] = 0;
                            window.score = window.score + field[i][j];
                            window.count ++;
                        }
                        break;
                    }
                }
            }
        }
    }
    console.log(score);
    $("input").val(score);
    return field;
}
function RightDown(field){
    for (var i = 0; i < 4; i++){
        for(var j = 3; j > 0; j--) {
            for (var k = j - 1; k >= 0; k--)
            {
                if (field[i][k] != 0)
                {
                    if (field[i][j] == 0)
                    {
                        field[i][j] = field[i][k];
                        field[i][k] = 0;
                        window.count++;
                    }
                    else
                    {
                        if (field[i][j] ==field[i][k])
                        {
                            field[i][j] += field[i][k];
                            field[i][k] = 0;
                            window.score = window.score + field[i][j];
                            window.count++;
                        }
                        break;
                    }
                }
            }
        }
    }
    console.log(score);
    $("input").val(score);
    return field;
}
$(document).ready(function() {
    window.score = 0;
    StartData(window.field);
    $('<input>',{
        'value': window.score
    }).insertAfter('div.col-md-8');

$(document).keydown(function(key){
    switch(parseInt(key.which,10)) {
        case 37:
            //left
            console.log('left');
            window.field = LeftUp(window.field);
            if (window.count>0){
                RandomCell();
            }

            UpdateCalls();
            window.count=0;
            Looser(window.field);
            break;
        case 38:
            //up
            var arr = Transp(window.field);
            window.field = Transp(LeftUp(arr));
            if (window.count>0){
                RandomCell();
            }
            UpdateCalls();
            window.count=0;
            Looser(window.field);
    break;
    case 39:
            //right
            console.log('right');
            window.field = RightDown(window.field);

            if (window.count>0){
                RandomCell();
            }
            UpdateCalls();
            window.count=0;
            Looser(window.field);
        break;
    case 40:
            //down
            console.log('down');
            var arr = Transp(window.field);
            window.field = Transp(RightDown(arr));
            if (window.count>0){
                RandomCell();
            }
            UpdateCalls();
            window.count=0;
            Looser(window.field);
        break;
    }
});

});