__author__ = 'user'

from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
import views

urlpatterns = [
    url(r'^$', views.PlayView.as_view(), name='play'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
