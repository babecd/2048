from django.shortcuts import render, render_to_response
from django.views.generic import View, TemplateView


class PlayView(TemplateView):
    template_name = 'play/play_page.html'

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {"count": list(range(4))})